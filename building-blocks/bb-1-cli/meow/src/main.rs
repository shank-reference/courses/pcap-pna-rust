#[macro_use]
extern crate clap;

#[macro_use]
extern crate dotenv_codegen;

use clap::App;

fn main() {
    let yaml = load_yaml!("cli.yml");
    let matches = App::from_yaml(yaml).get_matches();

    println!("INPUT (from cli): {}", matches.value_of("INPUT").unwrap());

    println!("PORT (from .env): {}", dotenv!("PORT"));
}
