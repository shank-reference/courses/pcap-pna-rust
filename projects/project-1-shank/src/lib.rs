#![deny(missing_docs)]

//! kvs - a key value store (blazing fast?)

use std::collections::HashMap;

/// The actual key-value store ([`KvStore`]).
///
/// [`KvStore`]: kvs::KvStore
pub struct KvStore {
    data: HashMap<String, String>,
}

impl KvStore {
    /// Creates an empty [`KvStore`]
    ///
    /// [`KvStore`]: crate::KvStore
    ///
    /// # Examples
    ///
    /// ```
    /// use kvs::KvStore;
    /// let store = KvStore::new();
    /// ```
    pub fn new() -> Self {
        Self {
            data: HashMap::new(),
        }
    }

    /// Sets the value for the key in the key-value store.
    ///
    /// # Examples
    ///
    /// ```
    /// use kvs::KvStore;
    ///
    /// let mut store = KvStore::new();
    /// store.set("key1".to_owned(), "value1".to_owned());
    /// assert_eq!(store.get("key1".to_owned()).unwrap(), "value1".to_owned());
    /// ```
    pub fn set(&mut self, key: String, value: String) {
        self.data.insert(key, value);
    }

    /// Sets the value for the key in the key-value store.
    ///
    /// # Examples
    ///
    /// ```
    /// use kvs::KvStore;
    ///
    /// let mut store = KvStore::new();
    /// store.set("key1".to_owned(), "value1".to_owned());
    /// assert_eq!(store.get("key1".to_owned()).unwrap(), "value1".to_owned());
    /// ```
    pub fn get(&self, key: String) -> Option<String> {
        self.data.get(&key).cloned()
    }

    /// Sets the value for the key in the key-value store.
    ///
    /// # Examples
    ///
    /// ```
    /// use kvs::KvStore;
    ///
    /// let mut store = KvStore::new();
    /// store.set("key1".to_owned(), "value1".to_owned());
    /// store.remove("key1".to_owned());
    /// assert_eq!(store.get("key1".to_owned()), None);
    /// ```
    pub fn remove(&mut self, key: String) {
        self.data.remove(&key);
    }
}
