extern crate structopt;

use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(
    name = env!("CARGO_PKG_NAME"),
    version = env!("CARGO_PKG_VERSION"),
    about = env!("CARGO_PKG_DESCRIPTION"),
    author = env!("CARGO_PKG_AUTHORS")
)]
struct Opt {
    #[structopt(subcommand)]
    cmd: Command,
}

#[derive(StructOpt, Debug)]
enum Command {
    #[structopt(about = "set key value")]
    Set { key: String, value: String },

    #[structopt(about = "get value for key")]
    Get { key: String },

    #[structopt(about = "remove key and associated value")]
    Rm { key: String },
}

fn main() {
    let opt = Opt::from_args();

    let _store = kvs::KvStore::new();

    match opt.cmd {
        Command::Set {
            key: _key,
            value: _value,
        } => {
            eprintln!("unimplemented");
            std::process::exit(1);
        }
        Command::Get { key: _key } => {
            eprintln!("unimplemented");
            std::process::exit(1);
        }
        Command::Rm { key: _key } => {
            eprintln!("unimplemented");
            std::process::exit(1);
        }
    };
}
